package com.kuubstudios.saturationhud;

import java.util.EnumSet;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.GuiChat;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.potion.Potion;
import net.minecraft.util.FoodStats;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * SaturationHUD
 * 
 * GUISaturationHud
 * 
 * @author Goz3rr
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

@SideOnly(Side.CLIENT)
public class GuiSaturationHud extends Gui implements ITickHandler {
	private final Minecraft mc;
	//private int updateCounter = 0;
	
	public GuiSaturationHud(Minecraft minecraft)
	{
		this.mc = minecraft;
	}
	
	@Override
	public void tickStart(EnumSet<TickType> type, Object... tickData) {
		//updateCounter++;
	}

	@Override
	public void tickEnd(EnumSet<TickType> type, Object... tickData) {
		if(type.contains(TickType.RENDER)) {
			RenderSaturationBar();
		}	
	}

	@Override
	public EnumSet<TickType> ticks() {
		return EnumSet.of(TickType.RENDER);
	}

	@Override
	public String getLabel() {
		return "Saturation HUD Renderer";
	}
	
	public void RenderSaturationBar()
	{
		// if ((mc.inGameHasFocus || mc.currentScreen == null || (mc.currentScreen instanceof GuiChat && showInChat)) && !mc.gameSettings.showDebugInfo && !mc.gameSettings.keyBindPlayerList.pressed)
		if (this.mc.thePlayer != null && this.mc.playerController.shouldDrawHUD() && (mc.inGameHasFocus || mc.currentScreen == null || mc.currentScreen instanceof GuiChat) && !mc.gameSettings.showDebugInfo && !mc.gameSettings.keyBindPlayerList.pressed)
		{
			//this.mc.mcProfiler.endStartSection("food");
			
			GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, this.mc.renderEngine.getTexture("/gui/icons.png"));
			//Random rand = new Random();
			
			ScaledResolution var5 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
	        int var6 = var5.getScaledWidth();
	        int var7 = var5.getScaledHeight();
			int var47 = var7 - 39;
			
			FoodStats var15 = this.mc.thePlayer.getFoodStats();
            //int var16 = var15.getFoodLevel();
            //int var17 = var15.getPrevFoodLevel();
            int var19 = var6 / 2 + 91;
			
			for (int var25 = 0; var25 < 10; ++var25)
            {
                int var51 = 16;
                byte var52 = 0;

                if (SaturationHud.drawHunger == true)
                {
	                if (this.mc.thePlayer.isPotionActive(Potion.hunger))
	                {
	                    var51 += 36;
	                    var52 = 13;
	                }
                }

                int var29 = var19 - var25 * 8 - 9;
                if (var25 < (var15.getSaturationLevel() / 20 * 10)) {
    	        	this.mc.ingameGUI.drawTexturedModalRect(var29 + SaturationHud.xOffset, var47 + SaturationHud.yOffset, 16 + var52 * 9 + 108, 27, 9, 9);
    	        	this.mc.ingameGUI.drawTexturedModalRect(var29 + SaturationHud.xOffset, var47 + SaturationHud.yOffset, var51 + 36, 27, 9, 9);
    	        }                                              
            }
			
			//this.mc.mcProfiler.endSection();				
		}
	}	
}
