package com.kuubstudios.saturationhud;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;

/**
 * SaturationHUD
 * 
 * SaturationHudClientProxy
 * 
 * @author Goz3rr
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

public class SaturationHudClientProxy extends SaturationHudCommonProxy
{
    @Override
    public void registerRenderers()
    {
        TickRegistry.registerTickHandler(new GuiSaturationHud(FMLClientHandler.instance().getClient()), Side.CLIENT);
    }
}