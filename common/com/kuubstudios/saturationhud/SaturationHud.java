package com.kuubstudios.saturationhud;

import net.minecraftforge.common.Configuration;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

/**
 * SaturationHUD
 * 
 * SaturationHud
 * 
 * @author Goz3rr
 * @license Lesser GNU Public License v3 (http://www.gnu.org/licenses/lgpl.html)
 * 
 */

@Mod(modid = "SaturationHud", name = "SaturationHud", version = "0.1 For MC1.4.6")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class SaturationHud {
	
	public static int xOffset = 0;
	public static int yOffset = 0;
	
	public static boolean drawHunger = false;
	
	@Instance("SaturationHud")
    public static SaturationHud instance;
	
    @SidedProxy(clientSide = "com.kuubstudios.saturationhud.SaturationHudClientProxy", serverSide = "com.kuubstudios.saturationhud.SaturationHudCommonProxy")
    public static SaturationHudCommonProxy proxy;

    @PreInit
    public void preInit(FMLPreInitializationEvent event)
    {
    	Configuration config = new Configuration(event.getSuggestedConfigurationFile());
        config.load();
        xOffset = config.get("General", "xOffset", 0).getInt();
        yOffset = config.get("General", "yOffset", 0).getInt();   
        
        drawHunger = config.get("General", "drawHunger", false).getBoolean(false);
        config.save();
    }

    @Init
    public void load(FMLInitializationEvent event)
    {
    	proxy.registerRenderers();
    }

    @PostInit
    public void postInit(FMLPostInitializationEvent event)
    {

    }
}
